/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInformation() {
		alert("Hi! Please fill out the following information:");
		let fullName = prompt("Enter full name:");
		let age = prompt("Enter age:");
		let address = prompt("Address:");

		console.log("Hi", fullName);
		console.log(fullName + "'s age is", age);
		console.log(fullName, "is located at", address);
	};

	userInformation();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topBands() {
		const bands = ["HAIM", "Bleachers", "The Aces", "Fallout Boy", "Kamikazee"];
		console.log("Top 5 Bands:", bands)
	};

	topBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topMovies() {
		const movies = ["Brave (2012)", "Everything Everywhere All At Once (2022)", "Children of Men (2006)", "Avengers: End Game (2019)", "La La Land (2016)"];
		const ratings = ["78%", "95%", "92%", "94%", "91%"];

		function withRatings(top, movie, rating){
			console.log(top + ".", "Title:",movie);
			console.log("Tomatometer for",movie + ":", rating);
		};

		for(i=0; i<5; i++){
			withRatings(i+1, movies[i], ratings[i]);
		};
	};

	topMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

// console.log(friend1); 
// console.log(friend2); 

printFriends();
