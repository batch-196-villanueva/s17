console.log('hi');

// Functions
	// Functions are lines/ block of codes that tell our device/ application to perform a certain task when called or invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function
/*
	Syntax:
		function functionName(){
			code block(statement)
		}

>> function keyword - used to defined a javascript functions
>> functionName - the function name. Functions are named to be able to use later in the code.
>> function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

*/

function printName(){
	console.log('My name is Anna');
};
// Function invocation
	//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.

	//It is common to use the term "call a function" instead of "invoke a function".
printName();
// result: My name is Anna 

// declaredFunction();
// result: err, because it is not yet defined

// Function Declaration vs. Expressions
	//A function can be created through function declaration by using the function keyword and adding a function name.

	//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).


declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

function declaredFunction(){
	console.log("Hi I am from declaredFunction()");
};

declaredFunction();

// Function Expression
	//A function can also be stored in a variable. This is called a function expression.
	//A function expression is an anonymous function assigned to the variableFunction

	// Anonymous function- function without a name


let variableFunction = function(){
	console.log('I am from variableFunction');	
};

variableFunction();

// We can also create a function expression of a named function.
// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
// Function Expressions are always invoked (called) using the variable name.
let funcExpression = function funcName(){
	console.log('Hello from the other side');
};

funcExpression();

// You can reassign declared functions and function expression to new anonymous function

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log('Updated funcExpression');
};

funcExpression();

const constantFunction = function(){
	console.log('Initialized with const');
};

constantFunction();

// constantFunction = function(){
// 	console.log('Cannot be reassigned!')
// }

// constantFunction();
// reassignment with const function expression is not possible

// Function Scoping

/*
Scope is the accessibility (visibility) of variables within our program.
	Javascript Variables has 3 types of scope:
	1. local/ block scope
	2. global scope
	3. function scope

*/

{
	let localVar = "Armando Perez";
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);
// console.log(localVar); // result: err

//Function Scope
	/*		
		JavaScript has function scope: Each function creates a new scope.
		Variables defined inside a function are not accessible (visible) from outside the function.
		Variables declared with var, let and const are quite similar when declared inside a function.
	*/


function showNames(){
	// function scoped variable
	var functionVar = 'Joe';
	const functionConst = 'Nick';
	let functionLet = 'Kevin';
	
	console.log(functionVar);	
	console.log(functionConst);	
	console.log(functionLet);	
};

showNames();

// result: err, the variables, functionVar,functionConst and functionLet, are function scoped and cannot be accessed outside of the function they were declared in.
// console.log(functionVar);	
// console.log(functionConst);	
// console.log(functionLet);

// Nested Function
	//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

function myNewFunction(){
	let name = 'Yor';

	function nestedFunction(){
		let nestedName = 'Brando';
		console.log(nestedName);
	};
	// console.log(nestedName); result: err, nestedName for functions scope
	nestedFunction();
};
myNewFunction();

// Function and Global Scoped Variable

// Global Scoped variable
let globalName = 'Alan';

function myNewFunction2(){
	let nameInside = "Marco";

	//Variables declared Globally (outside any function) have Global scope.
	//Global variables can be accessed from anywhere in a Javascript program including from inside a function.
	console.log(globalName);
}
myNewFunction2();
// console.log(nameInside); result: err, nameInside is function scoped. It cannot be accessed globally.

// alert()
/*
	Syntax:
		alert('message');
*/
// run immediately once the page loads
// alert('Hello World');

function showSampleAlert(){
	alert('Hello User!');	
};

console.log("I will only log in the console when the alert is dismissed.");

// prompt()
/*
	Syntax:
		prompt("<dialog>")
*/

// let samplePrompt = prompt('Enter your name:');
// console.log('Hello ' + samplePrompt);

// let sampleNullPrompt = prompt("Don't enter anything");
// console.log(sampleNullPrompt);
// if the prompt() is cancelled, the result will be: null
// if there is no input in the prompt, the result will be: empty string

function printWelcomeMessage(){
	let firstName = prompt('Enter your first name:');
	let lastName = prompt('Enter your last name:');

	console.log('Hello, ' + firstName + " " + lastName + "!");
	console.log("Welcome to my page");
};

printWelcomeMessage();

// Function Naming Conventions
	// Function should be definitive of its task. Usually contains a verb

function getCourses(){
	let courses = ['Science 101,', 'Arithmetic 103', 'Grammar 105'];
	console.log(courses);
};

getCourses();

// Avoid generic names to avoid confusion
function get(){
	let name = "Anya";
	console.log(name);
};

get();

// Avoid pointless and inappropriate function names

function foo(){
	console.log(25 % 5);
}

foo();

// Name your functions in small caps. Follow camelcase when naming functions

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}

displayCarInfo();

